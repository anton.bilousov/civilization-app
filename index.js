const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require("body-parser");
const sio = require("socket.io");
const socketService = require("./server/socket.service");
const userValue = require('./db/users.json');

const bcrypt = require('bcrypt');
const saltRounds = 15;

const mongo = require('./server/modules/mongo.interface');

const app = express();

app.use((req, res, next) => {
    if (req.header('Authorization') || true) {
        next();        
    }
});

app.use( bodyParser.json() );

app.use(express.static(path.join(__dirname, 'dist/civilization')));

app.post('/sign', (req, res) => {
    mongo.findClient({login: req.body.login}).then(user => {
        if (!user) {
            let newClient;
            bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
                req.body.password = hash;
                newClient = req.body;
                newClient.userId = (Math.round(Math.random() * 1000)).toString();

                mongo.createClient(newClient).then(success => {
                    res.send(JSON.stringify({status: "Done"}));
                }, err => {
                    res.statusCode = 500;
                    return res.send({status: err});
                })
            });
        } else {
            res.statusCode = 500;
            res.send(JSON.stringify({status: "password or login incorrect"}));
        }
    })
});

app.post('/user', (req, res) => {
    mongo.findClient({login: req.body.login}).then(client => {
        if (client) {
            bcrypt.compare(req.body.password, client.password, function(err, result) {
                if (result) {
                    mongo.findUser({userName: client.userId}).then(users => {
                        if (users === null) {
                            const position =(Math.floor(Math.random() * 9));
                            userValue.userName = client.userId;
                            userValue.baseUnits.forEach(unit => {
                                unit.userId = client.userId;
                                unit.x = position;
                            });

                            mongo.createUser(userValue).then(user => {
                                res.send(JSON.stringify(client.userId));
                            }, err => {
                                res.statusCode = 500;
                                return res.send({status: err});
                            })
                        } else {
                            res.send(JSON.stringify(client.userId));
                        }
                    })
                } else {
                    res.statusCode = 500;
                    return res.send({status: "password or login incorrect"});
                }
            });
        } else {
            res.statusCode = 500;
            return res.send({status: "password or login incorrect"});
        }
    })
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/civilization/index.html'));
});

const port = '4500';
app.set('port', process.env.PORT || port);

const server = http.createServer(app);

const io = sio.listen(server);

io.sockets.on('connection', function (socket) {
    const ios = io;
    return socketService(socket, ios);
});

server.listen(process.env.PORT || port, () => console.log(`API running on localhost:${process.env.PORT || port}`));
