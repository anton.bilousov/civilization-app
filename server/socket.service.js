const landscapeService = require("./landscape");
const mongo = require('./modules/mongo.interface');

const enemyObject = (socket) => {
    return {
        userId: socket.userCustomId,
        units: socket.landscape.units.getUnits,
        cities: socket.landscape.city.getCity
    }
};
const socketHelper = (socket) => {
    socket.emit('unit', socket.landscape.units.getUnits);
    socket.emit('city', socket.landscape.city.getCity);
    socket.emit('metric', socket.landscape.metric);
    socket.in(socket.currentRoom).emit('enemy', enemyObject(socket));
};


const socketService = (socket, io) => {
    
    console.log('socket connect');

    socket.on('lobby', (message) => {
        socket.userCustomId = message.key;
        mongo.addUser(message.name, message.key).then(lobby => {
            socket.join(lobby.name);
            socket.landscape = new landscapeService(socket.userCustomId);
            io.of('/').in(lobby.name).clients((error,clients) => {
                clients.forEach(ele => {
                    const sendObj = {
                        name: lobby.name,
                        users: []
                    };
                    const user = io.sockets.connected[ele];

                    lobby.users.forEach(el => {
                        if (el.userName === user.userCustomId) {
                            sendObj.users.push({userName: el.userName,leader: el.leader, operation: true});
                        } else {
                            sendObj.users.push({userName: el.userName,leader: el.leader, operation: false});
                        }

                        return el;
                    });
                    user.emit('lobby', sendObj);
                });
            });
        }, (err) => socket.emit('err', err));
    });
    socket.on('start', (roomName) => {
        mongo.changeRoomStatus('close', roomName).then(lobby => {
            io.of('/').in(lobby.name).clients((error,clients) => {
                clients.forEach(ele => {
                    const user = io.sockets.connected[ele];

                    user.currentRoom = lobby.name;
                    user.emit('start');
                });
            });
        })
    });
    socket.on('user', () => {
        if (!(socket.userCustomId && socket.currentRoom)) return socket.emit('err', 'no active user');
        io.of('/').in(socket.currentRoom).clients((error,clients) => {
            let matrix;
            clients.forEach(ele => {

                const user = io.sockets.connected[ele];

                if (!matrix) {
                    matrix = io.sockets.connected[ele].landscape.getMatrix();
                }
                user.landscape.start(user, matrix)
            });
        });
    });

    socket.on('unit', () => {
        socket.emit('unit', socket.landscape.units.getUnits);
    });

    socket.on('status', () => {
        if (!(socket.userCustomId && socket.currentRoom)) {
            socket.emit('statusErr');
        }
    });

    socket.on('attackUnity', (attack) => {
        socket.emit('unit', socket.landscape.units.attackUnity(attack.from, attack.to));
        socket.in(socket.currentRoom).emit('enemy', enemyObject(socket));

        io.of('/').in(socket.currentRoom).clients((error,clients) => {
            clients.forEach(ele => {
                const user = io.sockets.connected[ele];

                if (user.userCustomId === attack.to.userId) {
                    user.emit('unit', user.landscape.units.defenceUnit(attack.to, attack.from));
                    user.in(user.currentRoom).emit('enemy', enemyObject(user));
                }
            });
        });
    });

    socket.on('siege', (siege) => {
        socket.emit('unit', socket.landscape.units.moveMinus(siege.from));
        socket.in(socket.currentRoom).emit('enemy', enemyObject(socket));

        io.of('/').in(socket.currentRoom).clients((error,clients) => {
            clients.forEach(ele => {
                const user = io.sockets.connected[ele];

                if (user.userCustomId === siege.to.userId) {
                    const city = user.landscape.city.defenceCity(siege);
                    user.emit('city', user.landscape.city.getCity);
                    user.in(user.currentRoom).emit('enemy', enemyObject(user));

                    if (city) {
                        socket.landscape.city.cityTakeOver(city);
                        socket.emit('city', socket.landscape.city.getCity);
                        socket.in(socket.currentRoom).emit('enemyMove', enemyObject(socket));
                    }
                }
            });
        });
    });

    socket.on('unitCanMove', (move) => {
        const unitMotion = socket.landscape.units.unitCanMove(move.from, move.to);
        if (unitMotion.status) {
            socket.emit('unitMove', unitMotion.units);
            socket.in(socket.currentRoom).emit('enemyMove', {userId: socket.userCustomId, unitsMove: unitMotion.units});
        } else {
            socket.emit('err', 'No motion');
        }
    });

    socket.on('createCity', (unit) => {
        if (socket.landscape.city.canBuild(unit)) return;
        socket.landscape.createCity(unit);
        socketHelper(socket);
    });
    socket.on('createBuilding', (unit) => {
        const data = socket.landscape.createConstruction(unit);
        socket.emit('createBuilding', data);
        socket.emit('unit', socket.landscape.units.getUnits);
        socket.emit('city', socket.landscape.city.getCity);
        socket.in(socket.currentRoom).emit('createBuilding', data);
    });

    socket.on('nextMove', () => {
        io.of('/').in(socket.currentRoom).clients((error, clients) => {
            clients.forEach(ele => {
                const user = io.sockets.connected[ele];
                user.landscape.nextMove();
                socketHelper(user);
                socket.landscape.technologyChange(socket);
            });
        });
    });

    socket.on('canBuildOn', item => {
        socket.emit('canBuildOn', socket.landscape.canBuildOn(item));
    });

    // socket.on('rebase', () => {
    //     socket.emit('getMatrix',socket.landscape ? socket.landscape.matrix : [[]]);
    //     socket.emit('unit', socket.landscape ? socket.landscape.units.getUnits : []);
    //     socket.emit('city', socket.landscape ? socket.landscape.city.getCity : []);
    // });

    socket.on('cityData', (id) => {
        socket.emit('cityData', socket.landscape.city.getCityById(id));
        socket.landscape.buildings(socket);
    });

    socket.on('addToProd', (building) => {
        socket.landscape.city.addToProd(building);
        socket.emit('cityData', socket.landscape.city.getCityById(building));
    });

    socket.on('technology', (tech) => {
        socket.landscape.addTechnology(tech, socket);
    });

    socket.on('save', () => {
        socket.emit('save', socket.landscape.save(socket.userCustomId));
    });

    socket.on('disconnect', function() {
        console.log('socket disconnect');
        if (!(socket.currentRoom && socket.userCustomId)) return;
        io.of('/').in(socket.currentRoom).clients((error,clients) => {
            if (clients.length >= 1) {
                mongo.deleteFromLobby(socket.currentRoom, socket.userCustomId).then(() => {
                    console.log('Delete');
                });
            } else {
                mongo.deleteLobby(socket.currentRoom);
            }
            mongo.deleteGameSession(socket.userCustomId);
        })
    });
};

module.exports = socketService;
