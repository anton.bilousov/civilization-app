class CreatUnit {
    constructor(el, id) {
        this.userId = id;
        this.x = el.x;
        this.y = el.y;
        this.img = el.processing.img;
        this.hp = 100;
        this.basePoints = el.processing.basePoints;
        this.strength = el.processing.strength;
        this.move = el.processing.basePoints;
        this.unitType = el.processing.unitType;
        this.unitId = 1;
        this.children = 4;
        this.id = (Math.round(Math.random() * 100)).toString();
    }
}

module.exports = CreatUnit;
