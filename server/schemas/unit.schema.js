const mongoose = require('mongoose');

const users = new mongoose.Schema({
    userName: String,
    leader: String,
    baseUnits: [
        {
            x: Number,
            y: Number,
            userId: String,
            name: String,
            cost: Number,
            unitType: String,
            img: String,
            hp: Number,
            strength: Number,
            basePoints: Number,
            id: String,
            move: Number,
            maxHealth: Number,
        }
    ]
});

module.exports = users;
