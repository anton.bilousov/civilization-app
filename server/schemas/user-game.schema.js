const mongoose = require('mongoose');

const UserGame = new mongoose.Schema({
    userName: String,
    technology: [
        {
            techId: Number,
            name: String,
            image: String,
            cost: Number,
            inUse: Boolean,
            parent: Number,
            children: Number,
            buildingsId: Number,
            active: Boolean
        }
    ],
    buildings: [
        {
            techId: String
        }
    ]
});

module.exports = UserGame;
