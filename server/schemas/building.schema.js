const mongoose = require('mongoose');

const building = new mongoose.Schema({
    name: String,
    cost: Number,
    img: String,
    unitType: String,
    technologyId: String,
    bonuses: {
        method: String,
        value: Number
    }
});

module.exports = building;
