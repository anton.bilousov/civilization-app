const mongoose = require('mongoose');

const User = new mongoose.Schema({
    email: String,
    firstName: String,
    lastName: String,
    login: String,
    password: String,
    userId: String
});

module.exports = User;
