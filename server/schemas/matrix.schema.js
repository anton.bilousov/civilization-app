const mongoose = require('mongoose');

const matrix = new mongoose.Schema({
    userId: String,
    matrix:[
        [
            {
                x: Number,
                y: Number,
                resources: {
                  img: String,
                  name: String,
                  quantity: Number
                },
                img: String,
                food: Number,
                manufacture: Number,
                buildings: String,
                fieldType: String
            }
        ]
    ]
});

module.exports = matrix;
