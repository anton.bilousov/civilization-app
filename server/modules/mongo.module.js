const mongoose = require('mongoose');
const users = require('../schemas/unit.schema');
const client = require('../schemas/user.schema');
const building = require('../schemas/building.schema');
const matrix = require('../schemas/matrix.schema');
const lobby = require('../schemas/lobby.schema');
const gameSession = require('../schemas/user-game.schema');

const User = mongoose.model('User', users);
const Client = mongoose.model('Client', client);
const Buildings = mongoose.model('Buildings', building);
const Matrix = mongoose.model('Matrix', matrix);
const Lobby = mongoose.model('Lobby', lobby);
const GameSession = mongoose.model('GameSession', gameSession);
const buildings = require('../../db/buildings');


class MongoModule {
    constructor() {
        mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost:27017/test', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        this.db = mongoose.connection;

        this.db.on('error', console.error.bind(console, 'connection error:'));
        this.db.once('open', function() {
            console.log('db connected');
        });


    }

    addGameSession(user) {
        return new Promise((resolve, reject) => {
            const createSession = {
                userName: user,
                technology: [],
                buildings: [{techId: '0'}]
            };

            // createSession.users.push(user);

            const saveLobby = new GameSession(createSession);
            saveLobby.save(err => {
                if (err) reject(err);

                resolve(createSession);
            })
        })
    }

    modifyTechnologies(techList, user) {
        return new Promise((resolve, reject) => {
            GameSession.updateOne({userName: user}, techList, (err) => {
                if (err) reject(err);
                resolve(techList);
            })
        })
    }

    findGameSession(user) {
        return new Promise((resolve, reject) => {
            GameSession.findOne({userName: user}, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        })
    }

    deleteGameSession(user) {
        return new Promise((resolve, reject) => {
            GameSession.findOne({userName: user}, (err, item) => {
                if (item) item.remove();
                resolve('removed');
            })
        })
    }

    addRoom(name, user) {
        return new Promise((resolve, reject) => {
            const createLobby = {
                name,
                status: 'pending',
                users: []
            };

            createLobby.users.push(user);

            const saveLobby = new Lobby(createLobby);
            saveLobby.save(err => {
                if (err) reject(err);

                resolve(createLobby);
            })
        })
    }

    deleteRoom(name) {
        return new Promise((resolve, reject) => {

            Lobby.findOne({name}, (err, item) => {
                if (err) reject(err);
                if (item) item.remove();
                resolve('removed');
            })
        })
    }

    updateRoom(name, lobby, users) {
        return new Promise((resolve, reject) => {

            lobby.users.push(users);
            Lobby.updateOne({name}, lobby, (err) => {
                if (err) reject(err);
                resolve(lobby);
            });
        });
    }

    updateLobby(name, lobby) {
        return new Promise((resolve, reject) => {

            Lobby.updateOne({name}, lobby, (err) => {
                if (err) reject(err);
                resolve(lobby);
            });
        });
    }

    getRoomUsers(name) {
        return new Promise((resolve, reject) => {
            Lobby.findOne({name}, (err, rooms) => {
                if (err) reject(err);
                resolve(rooms);
            })
        });
    }

    getBuildings(ids) {
        return new Promise((resolve, reject) => {
            let send = [];

            ids.map((id) => {
                const data = buildings.filter((val) => {
                    if (val.technologyId === id) {
                        return val;
                    }
                });
                if (data.length) {
                    send = [...send, ...data];
                }
            });
            resolve(send);
        })
    }

    createMatrix(saveDate) {
        return new Promise((resolve, reject) => {
            const saveMatrix = new Matrix(saveDate);

            saveMatrix.save(err => {
                if (err) {
                    reject(err)
                } else {
                    resolve('Done');
                }
            })
        })
    }

    createClient(newClient) {
        return new Promise((resolve, reject) => {
            const createClient = new Client(newClient);

            createClient.save(err => {
                if (err) {
                    reject(err)
                } else {
                    resolve('Done');
                }
            })
        })
    }

    findClient(search) {
        return new Promise((resolve, reject) => {
            Client.findOne(search, (err, user) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(user);
                }
            })
        });
    }

    createUser(newClient) {
        return new Promise((resolve, reject) => {
            const createClient = new User(newClient);

            createClient.save(err => {
                if (err) {
                    reject(err)
                } else {
                    resolve('Done');
                }
            })
        })
    }

    findUser(search) {
        return new Promise((resolve, reject) => {
            User.findOne(search, (err, user) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(user);
                }
            })
        });
    }
}

module.exports = MongoModule;
