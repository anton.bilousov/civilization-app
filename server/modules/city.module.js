const HelperModule = require('./helper.module');
const getNewCity = require("../factories/factoryCity");

class CityMethods {
    static addFood(el, value) {
        el.food += value;
    }
    static addManufacture(el, value) {
        el.manufacture += value
    }
    static addCulture(el, value) {
        el.culture += value;
    }
}
class CityModule extends HelperModule {
    constructor(id, cities = []) {
        super();
        this.id = id;
        this.city = cities;
    }

    get getCity() {
        return this.city;
    }

    add(unit, matrix) {
        const city = new getNewCity(unit, this.id);

        this.getCityResources(city, matrix);

        this.city.push(city);

        return city;
    }

    cityTakeOver(city) {
        city.userId = this.id;
        this.city.push(city);
    }

    getCityById(cityId) {
        let city;
        for (let i = 0; i < this.city.length; i++) {
            if (this.city[i].id === cityId.id) {
                city = this.city[i];
                break;
            }
        }
        return city;
    }

    addToProd(prod) {
        this.city.find(el => {
            if (el.id === prod.id) {
                el.processing = prod.build;
                return true;
            }
        });
    }

    canBuild(city) {
        const closes = this.getCloses(city.x, city.y);
        return closes.some(el => {
            return this.city.some(element => {
                return (element.x === el.x) && (element.y === el.y)
            });
        });
    }

    defenceCity({from, to}) {
        const siegeCity = this.getCityById(to);
        let cityCatch;

        this.city = this.city.filter(city => {
            if (city.id === siegeCity.id) {
                city.hp -= from.strength;
                if (city.hp > 0) {
                    return city
                } else {
                    cityCatch = city;
                    return false;
                }
            } else {
                return city;
            }
        });

        return cityCatch;
    }

    getCityResources(city, matrix) {
        this.getCloses(city.x, city.y).forEach(el => {
            city.food += matrix[el.x][el.y].food;
            city.manufacture += matrix[el.x][el.y].manufacture;
            const res = matrix[el.x][el.y].resources;
            if (res) city.resources.push(res);
        });
    }

    cityRebase(matrix) {
        this.city.forEach(city => {
            city.food = 0;
            city.manufacture = 0;
            this.getCityResources(city, matrix);
        })
    }

    createBuilding(el) {
        CityMethods[el.processing.bonuses.method](el, el.processing.bonuses.value);
        el.buildings.push(el.processing);
    }

    nextMove(callback) {
        this.city.forEach(el => {
            el.foodLevel += el.food;

            if (el.foodLevel >= 100 ) {
                el.foodLevel -= 100;
                el.cityLevel++;
                el.science += 1;
                el.gold += 1;
                el.culture += 1;
                el.manufacture += 1;
                el.maxHealth += 20;
                el.hp = el.maxHealth;
            }
            if (el.hp < el.maxHealth) {
                el.hp += (el.maxHealth / 10);
            }
            if (el.processing) {
                el.processing.cost -= el.manufacture;
                if (el.processing.cost <= 0) {
                    el.processing.unitType === 'building' ? this.createBuilding(el) : callback.createUnit(el);
                    el.processing = null;
                }
            }
        });
    }
}

module.exports = CityModule;
