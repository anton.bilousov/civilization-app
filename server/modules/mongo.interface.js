const MongoModule = require('./mongo.module');

class Mongo extends MongoModule {

    constructor() {
        super();
    }

    deleteLobby(name) {
        this.deleteRoom(name).then(() => {
            console.log('Lobby deleted');
        })
    }


    deleteFromLobby(room, user) {
        return new Promise((resolve, reject) => {

            this.getRoomUsers(room).then(lobby => {
                lobby.users = lobby.users.filter(el => {
                    if (el.userName !== user) {
                        return el;
                    }
                });

                return this.updateLobby(room, lobby);
            }).then(() => {
                resolve('Delete');
            })
        })
    }

    addUser(name, user) {
        return new Promise((resolve, reject) => {
            this.getRoomUsers(name).then((lobby) => {
                if (lobby && lobby.status === 'close') {
                    reject('close');
                }
                this.findUser({userName: user}).then(users => {
                    if (users !== null) {
                        if (!lobby) {
                            this.addRoom(name, users).then(lobbys => resolve(lobbys));
                        }  else {
                            const item = lobby.users.find(el => {
                                return el.userName === user
                            });
                             ~item ?
                                this.updateRoom(name, lobby, users).then(lobbys => resolve(lobbys))
                                : resolve(lobby);
                        }
                    }
                })
            });
        });
    }

    changeRoomStatus(status, name) {
        return new Promise((resolve, reject) => {
            this.getRoomUsers(name).then((lobby) => {
                lobby.status = status;

                this.updateLobby(name, lobby).then(() => {
                    resolve(lobby)
                });
            });
        });
    }

    addTechnology(tech, id) {
        return new Promise((resolve, reject) => {
            this.findGameSession(id).then((data) => {
                data.technology = data.technology.map((val) => {
                    if (val.techId === tech.techId) {
                        val.inUse = true;
                        return val;
                    } else {
                        val.inUse = false;
                        return val;
                    }
                });
                this.modifyTechnologies(data, id).then(() => resolve(data.technology));
            })
        });
    }

    getAvailableBuildings(user) {
        return new Promise((resolve, reject) => {
            this.findGameSession(user).then((data) => {
                const datas = data.buildings.map((value) => {
                    return value.techId;
                });
                this.getBuildings(datas).then((val) => {
                    resolve(val);
                })
            })
        });
    }
}

module.exports = new Mongo();
