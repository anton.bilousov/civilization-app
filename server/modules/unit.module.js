const CreatUnit = require('../factories/factoryUnits');
const HelperModule = require('./helper.module');
const buildings = require('../../db/buildings');

class UnitModule extends HelperModule {
    constructor(id, units = []) {
        super();
        this.id = id;
        this.units = units
    }

    createUnit(el) {
        this.units.push(new CreatUnit(el, this.id));
    }

    delete(id) {
        this.units = this.units.filter(item => {
            if (item.id !== id) {
                return item;
            }
        });

        return this.units
    }

    motion(from, to, itr, unitsDid) {
        let some = this.getCloses(from.x, from.y);
        let number = {int: null};

        some.forEach(el => {
            if (el) {
                el.int = Math.abs(el.x - to.x) + Math.abs(el.y - to.y);
                if (number.int === null || number.int > el.int) {
                    number = el;
                }
            }
        });

        if (to.x !== from.x || to.y !== from.y) {
            if (!from.move) {
                return unitsDid.status = false;
            };
            from.oldx = from.x;
            from.oldy = from.y;
            from.move -= 1;
            unitsDid.units.push(Object.assign({}, from, number));
            itr++
            this.motion({...from, ...number}, to, itr, unitsDid);
        } else {
            return unitsDid.status = true;
        }
    }

    unitCanMove(from, to) {
        let itr = 1;
        const fromUnit = Object.assign({}, this.getUnitFromDb(from));
        const unitDidMove = {status: false, units: []};
        this.motion(fromUnit, to, itr, unitDidMove);  

        if (!unitDidMove.status) {
            return unitDidMove;
        }

        this.units = this.units.map(item => {
            if (item.id === fromUnit.id) {
                item.move -= itr;
                return Object.assign({}, item, to);
            } else {
                return item;
            }
        });
        return unitDidMove;
    }

    modify(data, socket) {
        this.units = this.units.map((unit) => {
            const dsasd = buildings.find((build) => {
                return build.unitId && (build.unitId === unit.children);
            });
            if (!dsasd) {
                return unit
            }
            console.log(dsasd, data);
            if (dsasd.technologyId === `${data.buildingsId}`) {
                console.log({...unit, ...dsasd});
                return {...unit, ...dsasd};
            }
        });
        socket.emit('unit', this.units);
    }

    canMove(from, to) {
        const fromEl = this.getUnitFromDb(from);
        if (to.unitType === 'unit') {
            to = this.getUnitFromDb(to)
        }
        if (fromEl.move === 0) return false;
        const closes = this.getCloses(fromEl.x, fromEl.y);
        return closes.some(item => {
            return (item.y === to.y) && (item.x === to.x);
        });
    }

    moveMinus(unit) {
        this.units = this.units.filter(el => {
            if (el.id === unit.id) {
                if (el.move > 0) el.move -= 1;
            }
            return el;
        });
        return this.units;
    }

    getUnitFromDb(from) {
        return this.units.find(item => {
            if (from.id === item.id) {
                return item;
            }
        });
    }

    defenceUnit(from, to) {
        if (to.move === 0) return this.units;
        this.units = this.units.filter(unit => {
            if (unit.id === from.id) {
                unit.hp -= to.strength;
                return unit.hp > 0 ? unit : false;
            } else {
                return unit;
            }
        });
        return this.units;
    }

    attackUnity(from) {
        const fromEl = this.getUnitFromDb(from);
        if (fromEl.move === 0) return this.units;

        this.units = this.units.filter(unit => {
            if (unit.id === from.id) {
                unit.move = 0;
                return unit;
            } else {
                return unit;
            }
        });

        return this.units;
    }

    reduceMoves(id) {
        this.units = this.units.map(item => {
            if (item.id === id) {
                item.move = 0;
                return item;
            }
            return item;
        });
    }

    restoreMoves() {
        this.units = this.units.map(item => {
            item.move = item.basePoints;
            return item;
        });

        return this.units;
    }

    get getUnits() {
        return this.units;
    }
}

module.exports = UnitModule;
