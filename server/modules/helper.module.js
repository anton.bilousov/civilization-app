const resources = require('../../db/resources');

class HelperModule {
    constructor() {
    }


    exc(x, y, temp) {
        if ((x >= 0 && y >= 0) && (x <= 9 && y <= 9) ) {
            temp.push({x: x, y: y})
        }
    }

    getCloses(x, y) {
        const temp = [];
        this.exc(x-1, y-1, temp);
        this.exc(x, y-1, temp);
        this.exc(x+1, y-1, temp);
        this.exc(x-1,y, temp);
        this.exc(x+1, y, temp);
        this.exc(x-1, y+1, temp);
        this.exc(x, y+1, temp);
        this.exc(x+1, y+1, temp);

        return temp;
    }

    static getRandomType(int) {
        return (Math.random() * 10) > int;
    }

    // TODO: rework
    get resources() {
        let send = resources[Math.round(Math.random() * (resources.length - 1))];
        const cost = Math.round(Math.random() * 10);
        send= {...send, quantity: cost};
        return send
    }

}

module.exports = HelperModule;
