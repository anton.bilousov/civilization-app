const CityModule = require('./modules/city.module');
const UnitModule = require('./modules/unit.module');
const mongo = require('./modules/mongo.interface');
const HelperModule = require('./modules/helper.module');
const techList = require('../db/technology');

const buildings = [
    {name: 'farm', type: 'grass', image: './assets/img/Farm.png', food: 1, manufacture: 0},
    {name: 'quarry', type: 'mountin', image: './assets/img/Quarry.png', food: 0, manufacture: 1}
    ];

class MountinField {
    constructor(resorces) {
        this.img = './assets/img/mountin.jpeg';
        this.food = 0;
        this.manufacture = 2;
        this.building = null;
        this.fieldType = 'mountin';
        this.resources = resorces;
    }
}
class FieldGrass{
    constructor(resorces) {
        this.img = './assets/img/grass.png';
        this.food = 1;
        this.manufacture = 1;
        this.building = null;
        this.fieldType = 'grass';
        this.resources = resorces;
    }
}

class LandscapeService  extends HelperModule {
    constructor(id) {
        super();
        this.id = id;
        this.city = new CityModule(this.id);
        this.matrix = [];
        this.metric = {
            currentGold: 0,
            gold: 0,
            science: 0,
            culture: 0,
            resources: []
        }
    }

    start(socket, matrix) {
        if (this.matrix.length === 0) {
            this.matrix = matrix
        }
        mongo.findUser({userName: this.id}).then((user) => {
            this.units = this.units || new UnitModule(this.id, user.baseUnits.map(user => user.toObject()));

            socket.emit('unit', this.units.getUnits);
            socket.emit('city', this.city.getCity);
            socket.emit('getMatrix', matrix);
            socket.in(socket.currentRoom).emit('enemy', {userId:this.id, units: this.units.getUnits, cities: this.city.getCity});
        });
        mongo.addGameSession(this.id).then((data) => {
            data.technology = techList.map((val) => {
                val.active = false;
                return val;
            });
            mongo.modifyTechnologies(data, this.id).then((list) => {
                socket.emit('technologies', list.technology);
            })
        })
    }

    getMatrix() {
        return this.matrix.length > 1 ? this.matrix : this.getNewMatrix();
    }

    createCity(unit) {
        this.units.delete(unit.id);

        this.matrix[unit.x][unit.y].building = 'city';

        const createdCity =  this.city.add(unit, this.matrix);

        // todo delete
        for (let key in this.metric) {
            if (this.metric.hasOwnProperty(key)) {
                if (!Array.isArray(this.metric[key])) {
                    createdCity[key] ? this.metric[key] += createdCity[key] : '';
                } else {
                    this.metric[key] = this.metric[key].concat(createdCity[key]);
                }
            }
        }
    }

    buildings(socket) {
        mongo.getAvailableBuildings(this.id).then(buildings => {
            socket.emit('available',  buildings);
        });
    }

    addTechnology(tech, socket) {
        mongo.addTechnology(tech, this.id).then(data => {
            socket.emit('technologies', data);
        })

    }

    getNewMatrix() {
        if (this.matrix.length > 1) return this.matrix;
        const arr = [];
        for (let index = 0; index < 10; index++) {
            const arrs = [];
            for (let s = 0; s < 10; s++) {
                const res = HelperModule.getRandomType(8) ? this.resources: null;

                if (HelperModule.getRandomType(9)) {
                    arrs.push(new MountinField(res));
                } else {
                    arrs.push(new FieldGrass(res));
                }
            }
            arr.push(arrs);
        }
        this.matrix = arr;
        return this.matrix ;
    }

    canBuildOn(item) {
        const place = this.matrix[item.x][item.y];
        return buildings.filter(elem => {
            if (elem.type === place.fieldType && place.building === null) {
                return elem;
            }
        });
    }

    createConstruction(data) {
        let unit = data.position;
        
        this.units.reduceMoves(unit.id);

        const str = buildings.find(element => {
            return (data.buildingType === element.name);
        });

        this.matrix[unit.x][unit.y].building = str.image;
        this.matrix[unit.x][unit.y].food += str.food;
        this.matrix[unit.x][unit.y].manufacture += str.manufacture;

        unit = {...unit, ...this.matrix[unit.x][unit.y]};
        this.city.cityRebase(this.matrix);
        return unit;
    }

    save(id) {
        const saveDate = {
            userId: id.user,
            matrix: this.matrix
        };

        mongo.createMatrix(saveDate).then(success  => {
            console.log(success);
        })
    }

    checkForUnitUpdate(tech, socket) {
        this.units.modify(tech, socket);
        socket.emit('')
    }
    // TODO: replace to mongo interface
    technologyChange(socket) {
        mongo.findGameSession(this.id).then((data) => {
            data.technology = data.technology.map(tech => {
                if (tech.inUse) {
                    tech.cost -= this.metric.science;
                    if (tech.cost <= 0) {
                        data.buildings.push({techId: tech.buildingsId});
                        tech.active = true;
                        tech.inUse = false;
                        this.checkForUnitUpdate(tech, socket);
                    }
                    return tech
                } else {
                    return tech;
                }
            });
            mongo.modifyTechnologies(data, this.id).then(() => {
                socket.emit('technologies', data.technology);
            });
        })
    }

    nextMove() {
        this.units.restoreMoves();
        this.city.nextMove(this.units);
        this.metric.currentGold += this.metric.gold;
    }
}

module.exports = LandscapeService;
