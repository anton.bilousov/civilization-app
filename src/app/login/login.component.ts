import {Component, ComponentFactoryResolver, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {AlertService} from "../shared-module/alert.service";
import {SignUpComponent} from '../sign-up/sign-up.component';


const validators = [Validators.required, Validators.maxLength(20)];
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public signIn = this.fb.group({
    login: ['', [...validators]],
    password: ['', [...validators]]
  });

  constructor(
      private http: HttpClient,
      private router: Router,
      private fb: FormBuilder,
      private resolver: ComponentFactoryResolver,
      private active: AlertService) {
  }

  ngOnInit() {
  }

  public goToSignUp() {
    this.router.navigate(['/sign-up']);
  }

  public submitUser() {
    if (this.signIn.valid) {
      this.signIn.disable();
      this.http.post('/user', this.signIn.value).subscribe((item: any) => {
        sessionStorage.setItem('id', item);
        this.router.navigate(['/game/lobby']);
      }, err => {
        this.signIn.enable();
        this.active.err(err.error.status);
      });
    }
  }

}
