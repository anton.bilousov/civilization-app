import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  private subject = new Subject();

  constructor() { }

  get onSubject() {
    return this.subject;
  }
  success(message) {
    this.subject.next({typeStyle: 'success', message});
  }

  err(message) {
    this.subject.next({typeStyle: 'err', message});
  }
}
