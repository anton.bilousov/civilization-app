import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges, DoCheck } from '@angular/core';
import {AlertService} from "../alert.service";

class Notification {
  style: string;
  closed: boolean;
  message: string;
  private time: number;

  constructor(mType, time = 5000) {
    this.style = mType.typeStyle;
    this.closed = false;
    this.message = mType.message;
    this.time = time;
    this.$timeout();
  }
  private $timeout() {
    setTimeout(() => {
      this.closed = true;
    }, this.time);
  }
}
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnDestroy, DoCheck {
  private onCall = this.alert.onSubject;
  public notifications: Array<any> = [];
  subscription: any;

  constructor(private alert: AlertService) {
    this.subscription = this.onCall.subscribe(callEvent => {
      this.notifications.push(new Notification(callEvent));
    });
  }

  ngDoCheck() {
    if (this.notifications.some(item => item.closed)) {
      this.notifications = this.notifications.filter(item => {
        return !item.closed;
      });
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
