import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  public signUpForm = this.fb.group(
      {
        firstName: ['', [Validators.required, Validators.pattern(/[a-zA-Z]+/), Validators.maxLength(5)]],
        lastName: ['', [Validators.required, Validators.pattern(/[a-zA-Z]+/)]],
        login: ['', [Validators.required, Validators.pattern(/\w+/)]],
        password: ['', Validators.required],
        email: ['', [Validators.required, Validators.pattern(/^[a-zA-Z0-9.-]+@[a-z]+\.[a-z]{2,4}$/)]]
      }
  );

  constructor(private fb: FormBuilder, private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  public onSubmit() {
    this.signUpForm.disable();
    this.http.post('/sign', this.signUpForm.value).subscribe((data: string) => {
        sessionStorage.setItem('id', data);
        this.router.navigate(['']);
    }, err => {
        this.signUpForm.enable();
    });
  }

}
