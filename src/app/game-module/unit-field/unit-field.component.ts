import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {IUnit} from '../game.interface';

@Component({
  selector: 'app-unit-field',
  templateUrl: './unit-field.component.html',
  styleUrls: ['./unit-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UnitFieldComponent {
  @Input() player: IUnit;
  @Output() unitOnClick = new EventEmitter<IUnit>();
  @Output() unitOnRightClick = new EventEmitter<IUnit>();

  constructor() {
  }

  public unitWillMove() {
    this.unitOnClick.emit(this.player);
  }

  public onRightClick() {
    this.unitOnRightClick.emit(this.player);
    return false;
  }

}
