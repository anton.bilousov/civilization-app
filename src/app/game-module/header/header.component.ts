import { Component, OnInit } from '@angular/core';
import {Store, select} from '@ngrx/store';
import {IAppState} from "../store/state/app.state";
import {selectMetric} from "../store/selectors/metric.selector";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public metric = this.store.pipe(select(selectMetric));

  constructor(private store: Store<IAppState>) {
  }

  ngOnInit() {

  }

}
