import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { GameComponent } from './game.component';
import { FieldComponent } from './field/field.component';
import { GameRoutingModule } from './game.routing.module';
import { UnitFieldComponent } from './unit-field/unit-field.component';
import { UnitDirective } from './directives/unit.directive';
import { InfoTabComponent } from './info-tab/info-tab.component';
import { CityComponent } from './city/city.component';
import { AddImageDirective } from './directives/image/add-image.directive';
import { SocketService } from './services/socket.service';
import { CityPageComponent } from './city-page/city-page.component';
import { ListOfBuildindsComponent } from './list-of-buildinds/list-of-buildinds.component';
import { HealthBarDirective } from './directives/health-bar/health-bar.directive';
import { StoreModule } from '@ngrx/store';
import { appReducer } from './store/reducers/app.reducer';
import { environment } from 'src/environments/environment';
import {HeaderComponent} from './header/header.component';
import {LobbyComponent} from './lobby/lobby.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared-module/shared.module';
import {MetricsPipe} from './pipes/metric/metrics.pipe';
import {TechnologyTabComponent} from './technology-tab/technology-tab.component';


@NgModule({
  declarations: [
    GameComponent,
    FieldComponent,
    UnitFieldComponent,
    UnitDirective,
    InfoTabComponent,
    CityComponent,
    AddImageDirective,
    CityPageComponent,
    ListOfBuildindsComponent,
    TechnologyTabComponent,
    HealthBarDirective,
    HeaderComponent,
    LobbyComponent,
    MetricsPipe
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    StoreModule.forRoot(appReducer),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    GameRoutingModule,
    SharedModule,
  ],
  providers: [SocketService]
})
export class GameModule { }
