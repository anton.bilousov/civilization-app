import { Component, OnInit } from '@angular/core';
import { SocketService } from './services/socket.service';
import { IField, IUnit } from './game.interface';
import { Store, select } from '@ngrx/store';
import { IAppState } from './store/state/app.state';
import { selectUnit } from './store/selectors/unit.select';
import { selectCity } from './store/selectors/city.selector';
import { selectMatrix } from './store/selectors/matrix.selector';
import { GetMatrix, ModifyMatrix } from './store/actions/matrix.action';
import {selectEnemyCity, selectEnemyUnits} from './store/selectors/enemy.selector';
import {Router} from '@angular/router';

type fieldOrUnit = IField | IUnit;
@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  public fieldMatrix = this.store.pipe(select(selectMatrix));
  public units = this.store.pipe(select(selectUnit));
  public processingEvent: IUnit;
  public cities = [];
  public citiesEnemy = this.store.pipe(select(selectEnemyCity));
  public unitsEnemy = this.store.pipe(select(selectEnemyUnits));

  constructor(private service: SocketService, private store: Store<IAppState>, private router: Router) {
    this.store.select(selectCity).subscribe((city) => {
      city.forEach(element => {
        const closes = this.service.getCloses(element.x, element.y);
        this.store.dispatch(new GetMatrix(closes));
      });
      this.cities = city;
    });

    this.service.socket.on('createBuilding', (item: IField) => {
      this.store.dispatch(new ModifyMatrix(item));
    });

    this.service.socket.on('statusErr', () => {
      this.router.navigate(['/game/lobby']);
    });
  }

  ngOnInit() {
    this.service.socket.emit('status');
  }

  public goToTech() {
    this.router.navigate(['game', 'technology']);
  }

  public processing(event: fieldOrUnit) {
    if (this.processingEvent) {
      this.service.socket.emit('unitCanMove', {from: this.processingEvent, to: event});
      this.processingEvent = null;
    }
  }

  public processingAdd(event) {
    this.processingEvent = event;
  }

  public processingOnClick(event) {
    if (this.processingEvent && this.processingEvent.userId !== event.userId) {
      if ((event as IUnit).unitType === 'unit') {
        this.service.socket.emit('attackUnity', {from: this.processingEvent, to: event});
        return;
      }
      this.processingEvent = null;
    }
  }

  public createCity() {
    if (this.processingEvent.unitType) {
      this.service.socket.emit('createCity', this.processingEvent);
      this.processingEvent = null;
    }
  }

  public citySiege(event) {
    if (this.processingEvent && this.processingEvent.userId !== event.userId) {
      this.service.socket.emit('siege',  {from: this.processingEvent, to: event});
      this.processingEvent = null;
    }
  }

  public saveGame() {
    this.service.socket.emit('save');
  }

  public createBuilding(type) {
    if (this.processingEvent.unitType) {
      this.service.socket.emit('createBuilding', {position: this.processingEvent, buildingType: type});

      this.processingEvent = null;
    }
  }

  public nextMove() {
    this.processingEvent = null;
    this.service.socket.emit('nextMove');
  }
}
