import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './game.component';
import { CityPageComponent } from './city-page/city-page.component';
import {LobbyComponent} from './lobby/lobby.component';
import {TechnologyTabComponent} from './technology-tab/technology-tab.component';

const routes: Routes = [{
    path: '',
    component: GameComponent
}, {
  path: 'city/:id',
  component: CityPageComponent
}, {
    path: 'lobby',
    component: LobbyComponent
}, {
    path: 'technology',
    component: TechnologyTabComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GameRoutingModule { }
