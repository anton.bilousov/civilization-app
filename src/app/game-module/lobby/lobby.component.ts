import { Component, OnInit } from '@angular/core';
import {SocketService} from "../services/socket.service";
import {Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit {
  // TODO: delete any
  public lobby: any;
  public currentRoom = this.fb.group({
    name: ['', Validators.pattern(/\w+/)]
  });

  constructor(private service: SocketService, private router: Router, private fb: FormBuilder) {
    this.service.socket.on('lobby', data => {
      this.lobby = data;
    });
    this.service.socket.on('start', (message) => {
      this.service.start();
      this.router.navigate(['/game']);
    });
  }

  ngOnInit() {
  }

  public onSubmit() {
    this.service.socket.emit('lobby', {
      key: sessionStorage.getItem('id'),
      name: this.currentRoom.value.name
    });
  }

  public onStart() {
    this.service.socket.emit('start', this.lobby.name);
  }

}
