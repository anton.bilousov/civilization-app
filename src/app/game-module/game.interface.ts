export interface IPosition {
    x: number;
    y: number;
}
export interface IUnit {
    x: number;
    y: number;
    userId: string;
    name: string;
    cost: number;
    unitType: string;
    img: string;
    hp: number;
    strength: number;
    basePoints: number;
    id: string;
    move?: number;
    maxHealth?: number;
    oldx?: number;
    oldy?: number;
}
interface IBuildingBonuses {
    method: string;
    value: number;
}
export interface IBuilding {
    name: string;
    cost: number;
    img: string;
    unitType: string;
    bonuses: IBuildingBonuses;
}

export interface ICity {
    userId: string;
    img: string;
    cityLevel: number;
    x: number;
    y: number;
    hp: number;
    foodLevel: number;
    food: number;
    gold: number;
    culture: number;
    science: number;
    manufacture: number;
    id: string;
    buildings: Array<IBuilding>;
    processing?: IUnit;
    maxHealth?: number;
}

export interface IField {
    x: number;
    y: number;
    food: number;
    manufacture: number;
    building: string;
    img: string;
    color?: string;
    resources: IResources;
}

export interface ITechnologies {
    techId: number;
    name: string;
    image: string;
    cost: number;
    parent: number;
    children: number;
    active?: boolean;
    inUse?: boolean;
}

export interface IResources {
    name: string;
    img: string;
    quantity: number;
}
