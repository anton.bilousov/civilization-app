import { Directive, Input, OnInit, ElementRef } from '@angular/core';
import {ICity, IUnit} from '../../game.interface';

@Directive({
  selector: '[appHealthBar]'
})
export class HealthBarDirective implements OnInit {
  @Input('appHealthBar') healthCheck: IUnit | ICity;

  public unitHealth: number;

  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
    const maxHp = this.healthCheck.maxHealth || 100;
    this.unitHealth = ((this.healthCheck.hp < 0 ? 0 : this.healthCheck.hp) * 100) / maxHp;

    this.elementRef.nativeElement.style.width = `${this.unitHealth}%`;
    if (this.healthCheck.userId !== sessionStorage.getItem('id')) {
      this.elementRef.nativeElement.style.backgroundColor = `#ce1111`;
    }
  }

}
