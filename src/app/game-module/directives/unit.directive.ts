import { Directive, Input, ElementRef, OnInit } from '@angular/core';
import { IUnit } from '../game.interface';

@Directive({
  selector: '[appUnit]'
})
export class UnitDirective implements OnInit {
  @Input('appUnit') unit: IUnit;

  private static check(oldValue, newValue) {
    return (oldValue || oldValue === 0) && oldValue !== newValue;
  }

  private static valueXY(oldValue, newValue) {
    return oldValue || oldValue === 0 ? oldValue : newValue;
  }

  constructor(private ref: ElementRef) {}

  private styleTop(i, steps) {
      this.ref.nativeElement.style.top = `${(((this.unit.oldx * 10) - (i * steps)) * 10) + 25}px`;
  }

  private styleLeft(i, steps) {
    this.ref.nativeElement.style.left = `${((this.unit.oldy * 10) - (i * steps)) * 10}px`;
  }

  private iterator() {
    const stepX = this.unit.oldx > this.unit.x ? 1 : -1;
    const stepY = this.unit.oldy > this.unit.y ? 1 : -1;
    let counter = 1;

    const time =  setInterval(() => {
      if (UnitDirective.check(this.unit.oldx, this.unit.x)) this.styleTop(counter, stepX);
      if (UnitDirective.check(this.unit.oldy, this.unit.y)) this.styleLeft(counter, stepY);
      counter += 1;
      if (counter > 10) clearTimeout(time);
    }, 50);
  }

  ngOnInit() {
    this.ref.nativeElement.style.position = 'absolute';
    this.ref.nativeElement.style.top = `${(UnitDirective.valueXY(this.unit.oldx, this.unit.x) * 100) + 25}px`;
    this.ref.nativeElement.style.left = `${UnitDirective.valueXY(this.unit.oldy, this.unit.y) * 100}px`;
    this.iterator();
  }
}

