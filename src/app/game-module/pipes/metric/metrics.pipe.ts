import { Pipe, PipeTransform } from '@angular/core';
import {IResources} from "../../game.interface";

@Pipe({
  name: 'metrics'
})
export class MetricsPipe implements PipeTransform {

  transform(value: Array<IResources>, ...args: any[]): Array<IResources> {
    if (!value) return null;

    let resources: Array<IResources>;
    resources = value.reduce((oldValue: Array<IResources>, newValue: IResources) => {
      const resource = oldValue.find((el) => {
        return el.name === newValue.name
      });
      if (resource) {
        resource.quantity += newValue.quantity;
      } else {
        oldValue.push(newValue)
      }
      return oldValue;
    }, []);

    return resources;
  }

}
