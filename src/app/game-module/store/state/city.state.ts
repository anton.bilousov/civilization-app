import { ICity } from '../../game.interface';

export interface ICityState {
    city: Array<ICity>;
}

export const initialCityState: ICityState = {
    city: []
};
