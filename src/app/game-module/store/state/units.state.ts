import { IUnit } from '../../game.interface';

export interface IUnitState {
    units: Array<IUnit>;
}

export const initiallUnitState: IUnitState = {
    units: []
};
