import {ITechnologies} from '../../game.interface';

export interface ITechnologiesState {
    technologies: Array<ITechnologies>;
}

export const initialTechnologiesState: ITechnologiesState = {
    technologies: []
};
