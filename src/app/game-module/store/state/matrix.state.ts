import { IField } from '../../game.interface';

export interface IMatrixState {
    matrix: Array<Array<IField>>;
}

export const initialMatrixState: IMatrixState = {
    matrix: [[]]
};
