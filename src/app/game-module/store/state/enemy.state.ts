import {ICity, IUnit} from "../../game.interface";

export interface IEnemyState {
    enemy: {
        [user: string]: {
            units: Array<IUnit>;
            city: Array<ICity>
        }
    }
}

export const initialEnemyState: any = {
    enemy: {}
};

