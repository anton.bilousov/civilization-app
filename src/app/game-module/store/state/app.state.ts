import { IUnitState, initiallUnitState } from './units.state';
import { initialCityState, ICityState } from './city.state';
import { IMatrixState, initialMatrixState } from './matrix.state';
import {IEnemyState, initialEnemyState} from './enemy.state';
import {IMetricState, initialMetricState} from './metric.state';
import {initialTechnologiesState, ITechnologiesState} from './technology.state';


export interface IAppState {
    matrix: IMatrixState;
    units: IUnitState;
    city: ICityState;
    enemy: IEnemyState;
    metric: IMetricState;
    technologies: ITechnologiesState;
}

export const initialAppState: IAppState = {
    matrix: initialMatrixState,
    units: initiallUnitState,
    city: initialCityState,
    enemy: initialEnemyState,
    metric: initialMetricState,
    technologies: initialTechnologiesState
};

export function getInitialState(): IAppState {
    return initialAppState;
}
