import { IAppState } from '../state/app.state';
import { createSelector } from '@ngrx/store';
import { IUnitState } from '../state/units.state';

const unitInState = (state: IAppState) => state.units;

export const selectUnit = createSelector(
    unitInState,
    (state: IUnitState) => state.units
    );

export const selectUnitId = createSelector(
    unitInState,
    (state: IUnitState, id) => state.units.find((el) => {
        if (el.id === id) {
            return true;
        }
    })
);
