import { IAppState } from '../state/app.state';
import { createSelector } from '@ngrx/store';
import { IMatrixState } from '../state/matrix.state';

const matrixInSate = (state: IAppState) => state.matrix;


export const selectMatrix = createSelector(
    matrixInSate,
    (state: IMatrixState) => state.matrix
);

