import {IAppState} from "../state/app.state";
import {createSelector} from '@ngrx/store';
import {IEnemyState} from "../state/enemy.state";

const enemySelectIn = (state: IAppState) => state.enemy;

export const selectEnemyUnits = createSelector(
    enemySelectIn,
    (state: any) => {
        let unit = [];
        for (let key in state.enemy) {
            unit.push(...state.enemy[key].units);
        }

        return unit;
    }
);

export const selectEnemyCity = createSelector(
    enemySelectIn,
    (state: any) => {
        const city = [];
        for (let key in state.enemy) {
            if (!state.enemy[key].cities) return ;
            city.push(...state.enemy[key].cities);
        }

        return city ? city : [];
    }
);
