import {IMetricState} from "../state/metric.state";
import {createSelector} from '@ngrx/store';
import {IAppState} from "../state/app.state";


const metricInState = (state: IAppState) => state.metric;

export const selectMetric = createSelector(
    metricInState,
    (state: IMetricState) => state.metric
);
