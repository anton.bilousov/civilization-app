import { IAppState } from '../state/app.state';
import { createSelector } from '@ngrx/store';
import {ITechnologiesState} from '../state/technology.state';


const technologiesInState = (state: IAppState) => state.technologies;

export const selectTechnology = createSelector(
    technologiesInState,
    (state: ITechnologiesState) => state.technologies
);
