import { IAppState } from '../state/app.state';
import { createSelector } from '@ngrx/store';
import { ICityState } from '../state/city.state';


const cityInState = (state: IAppState) => state.city;

export const selectCity = createSelector(
    cityInState,
    (state: ICityState) => state.city
    );
