import { Action } from '@ngrx/store';
import { ICity } from '../../game.interface';

export enum ECityAction {
    GetCity = 'City get',
    GetCitySuccess = 'Get city success'
}

export class GetCity implements Action {
    public readonly type = ECityAction.GetCity;
}

export class GetCitySuccess implements Action {
    public readonly type = ECityAction.GetCitySuccess;

    constructor(public payload: Array<ICity>) {}
}

export type CityActions = GetCity | GetCitySuccess;
