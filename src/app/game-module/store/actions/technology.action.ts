import { Action } from '@ngrx/store';
import {ITechnologies} from '../../game.interface';

export enum ETechnologiesAction {
    GetTechnologies = 'Technologies get',
    GetTechnologiesSuccess = 'Get Technologies success'
}

export class GetTechnologies implements Action {
    public readonly type = ETechnologiesAction.GetTechnologies;
}

export class GetTechnologiesSuccess implements Action {
    public readonly type = ETechnologiesAction.GetTechnologiesSuccess;

    constructor(public payload: Array<ITechnologies>) {}
}

export type TechnologiesActions = GetTechnologies | GetTechnologiesSuccess;
