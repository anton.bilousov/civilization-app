import { Action } from '@ngrx/store';


export enum EEnemyAction {
    GetEnemy = 'Get Enemy',
    GetEnemyMove = 'Get Enemy Move'
}
export class GetEnemy implements Action {
    public readonly type = EEnemyAction.GetEnemy;

    constructor(public payload: any ) {}
}

export class GetEnemyMove implements Action {
    public readonly type = EEnemyAction.GetEnemyMove;

    constructor(public payload: any ) {}
}

export type EnemyActions = GetEnemy | GetEnemyMove;
