import { Action } from '@ngrx/store';
import { IField } from '../../game.interface';

export enum EMatrixAction {
    GetMatrix = 'Get Matrix',
    ModifyMatrix = 'Matrix Modified',
    GetMatrixSuccess = 'Get Matrix Success'
}

export class ModifyMatrix implements Action {
    public readonly type = EMatrixAction.ModifyMatrix;
    constructor(public payload: any) {}
}

export class GetMatrix implements Action {
    public readonly type = EMatrixAction.GetMatrix;
    constructor(public payload: any) {}
}

export class GetMatrixSuccess implements Action {
    public readonly type = EMatrixAction.GetMatrixSuccess;

    constructor(public payload: Array<Array<IField>>) {}
}

export type MatrixActions = GetMatrix | GetMatrixSuccess | ModifyMatrix;
