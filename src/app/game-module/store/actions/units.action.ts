import { Action } from '@ngrx/store';
import { IUnit } from '../../game.interface';

export enum EUnitActions {
    GetUnits = 'Unit get',
    GetUnitsSuccess = 'Unit Get Success',
    GetUnitMove = 'Unit Move'
}

export class GetUnit implements Action {
    public readonly type = EUnitActions.GetUnits;
}

export class GetUnitsSuccess implements Action {
    public readonly type = EUnitActions.GetUnitsSuccess;
    constructor(public payload: Array<IUnit>) {}
}

export class GetUnitMove implements Action {
    public readonly type = EUnitActions.GetUnitMove;
    constructor(public payload: IUnit) {}
}

export type UnitActions = GetUnit | GetUnitsSuccess | GetUnitMove;
