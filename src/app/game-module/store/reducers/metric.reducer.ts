import {initialMetricState} from "../state/metric.state";
import {EMetricActions, MetricActions} from "../actions/metric.action";


export function metricReducer(state = initialMetricState, action: MetricActions) {
    switch (action.type) {
        case EMetricActions.GetMetrics:
            return {...state, metric: action.payload};
        default:
            return state;
    }
}
