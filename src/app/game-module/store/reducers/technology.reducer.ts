import {initialTechnologiesState, ITechnologiesState} from '../state/technology.state';
import {ETechnologiesAction, TechnologiesActions} from '../actions/technology.action';


export function technologiesReducer(
    state = initialTechnologiesState,
    action: TechnologiesActions): ITechnologiesState {
    switch (action.type) {
        case ETechnologiesAction.GetTechnologiesSuccess:
            return {
                ...state,
                technologies: action.payload
            };
        default:
            return state;
    }
}
