import { ActionReducerMap } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { unitReducer } from './units.reducer';
import { cityReducer } from './city.reducer';
import { matrixReducer } from './matrix.reducer';
import {enemyReducer} from './enemy.reducer';
import {metricReducer} from './metric.reducer';
import {technologiesReducer} from './technology.reducer';

export const appReducer: ActionReducerMap<IAppState, any> = {
    matrix: matrixReducer,
    units: unitReducer,
    city: cityReducer,
    enemy: enemyReducer,
    metric: metricReducer,
    technologies: technologiesReducer
};
