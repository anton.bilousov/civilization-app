import { initiallUnitState, IUnitState } from '../state/units.state';
import { UnitActions, EUnitActions } from '../actions/units.action';
import { IUnit } from '../../game.interface';

export function unitReducer(state = initiallUnitState, action: UnitActions): IUnitState {
    switch (action.type) {
        case EUnitActions.GetUnitsSuccess :
            return {
                ...state,
                units: action.payload
            };
        case EUnitActions.GetUnitMove :
            return {
                ...state,
                units: state.units.map((el: IUnit) => {
                    if (el.id === action.payload.id) {
                        return action.payload;
                    }
                    return el;
                })
            };
        default:
            return state;
    }
}
