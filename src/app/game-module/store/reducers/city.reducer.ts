import { initialCityState, ICityState } from '../state/city.state';
import { CityActions, ECityAction } from '../actions/city.action';

export function cityReducer(state = initialCityState, action: CityActions): ICityState {
    switch (action.type) {
        case ECityAction.GetCitySuccess:
            return {
                ...state,
                city: action.payload
            };
        default:
            return state;
    }
}
