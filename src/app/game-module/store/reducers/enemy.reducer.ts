import {initialEnemyState} from '../state/enemy.state';
import {EEnemyAction, EnemyActions} from '../actions/enemy.action';
import { IUnit } from '../../game.interface';

export function enemyReducer(state = initialEnemyState, action: EnemyActions) {
    switch (action.type) {
        case EEnemyAction.GetEnemy:
            state.enemy[action.payload.userId] = {
                        units: action.payload.units,
                        cities: action.payload.cities
                    };
            return {...state};
        case EEnemyAction.GetEnemyMove:
            state.enemy[action.payload.userId].units = state.enemy[action.payload.userId].units.map((el: IUnit) => {
                if (el.id === action.payload.unitsMove.id) {
                    return action.payload.unitsMove;
                }
                return el;
            });
            return {...state};
        default:
            return state;
    }
}
