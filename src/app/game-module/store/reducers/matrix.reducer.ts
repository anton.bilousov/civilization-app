import { initialMatrixState } from '../state/matrix.state';
import { MatrixActions, EMatrixAction } from '../actions/matrix.action';


export function matrixReducer(state = initialMatrixState, action: MatrixActions) {
    switch (action.type) {
        case EMatrixAction.GetMatrixSuccess:
            return {
                ...state,
                matrix: action.payload
            };
        case EMatrixAction.GetMatrix:
            action.payload.forEach(el => {
                state.matrix[el.x][el.y] = { ...state.matrix[el.x][el.y], color: 'yellow'};
            });
            return state;
        case EMatrixAction.ModifyMatrix:
            state.matrix[action.payload.x][action.payload.y] = action.payload;
            return state;
        default:
            return state;
    }
}
