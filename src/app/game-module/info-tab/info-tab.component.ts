import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { IUnit } from '../game.interface';
import { Store, select } from '@ngrx/store';
import { IAppState } from '../store/state/app.state';
import { selectUnitId } from '../store/selectors/unit.select';
import { SocketService } from '../services/socket.service';

@Component({
  selector: 'app-info-tab',
  templateUrl: './info-tab.component.html',
  styleUrls: ['./info-tab.component.scss']
})
export class InfoTabComponent implements OnInit, OnChanges {
  @Input() unitIs: IUnit;
  @Output() create = new EventEmitter<boolean>();
  @Output() build = new EventEmitter<boolean>();
  public unit: IUnit;
  public constractions = [];

  constructor(private store: Store<IAppState>, private service: SocketService) {

  }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.unitIs) {
      this.store.pipe(select(selectUnitId, this.unitIs.id)).subscribe(el => {
        this.unit = el;
      });
      if (this.unitIs.unitType === 'builder') {
        this.service.socket.on('canBuildOn', (items: Array<any>) => {
          this.constractions = items;
        });
        this.service.socket.emit('canBuildOn', this.unitIs);
      } else {
        this.constractions = [];
      }
    }
  }

  public unitCanBuild() {
    this.service.socket.emit('canBuildOn', this.unitIs);
  }

  public cityCreate() {
    this.create.emit(true);
  }
  public createBuilding(item) {
    this.build.emit(item);
  }

}
