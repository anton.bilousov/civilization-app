import { Injectable } from '@angular/core';

import * as io from 'socket.io-client';
import {IField, IUnit, ICity, ITechnologies} from '../game.interface';
import { Store } from '@ngrx/store';
import { IAppState } from '../store/state/app.state';
import { GetUnitsSuccess, GetUnitMove } from '../store/actions/units.action';
import { GetCitySuccess } from '../store/actions/city.action';
import { GetMatrixSuccess } from '../store/actions/matrix.action';
import {GetEnemy, GetEnemyMove} from '../store/actions/enemy.action';
import {GetMetric} from '../store/actions/metric.action';
import {AlertService} from '../../shared-module/alert.service';
import { of, Subject } from 'rxjs';
import { delay, concatMap } from 'rxjs/operators';
import {GetTechnologiesSuccess} from '../store/actions/technology.action';


const connectionOptions =  {
  'force new connection' : true,
  reconnectionAttempts: 'Infinity',
  timeout : 10000,
  transports : ['websocket'],
  extraHeaders: {
    Authorization: '1112'
  }
};

@Injectable()
export class SocketService {
  private url = '/';
  public socket;
  private subject = new Subject();
  // private subject2 = new Subject();

  constructor(private store: Store<IAppState>, private popUp: AlertService) {
    this.socket = io(this.url, connectionOptions);

    this.socket.on('getMatrix', (message: Array<Array<IField>>) => {
      this.store.dispatch(new GetMatrixSuccess(message));
    });

    this.socket.on('unit', (message: Array<IUnit>) => {
      this.store.dispatch(new GetUnitsSuccess(message));
    });
    this.socket.on('unitMove', (message: Array<IUnit>) => {
      message.forEach(el => {
        this.subject.next(el);
      });
    });
    this.socket.on('enemyMove', (message: any) => {
      message.unitsMove.forEach(el => {
        this.subject.next({userId: message.userId, unitsMove: el});
      });
    });
    this.socket.on('city', (message: Array<ICity>) => {
      this.store.dispatch(new GetCitySuccess(message));
    });
    // TODO: delete any
    this.socket.on('enemy', (message: any) => {
        this.store.dispatch(new GetEnemy(message));
    });
    this.socket.on('technologies', (technologiesList: Array<ITechnologies>) => {
        this.store.dispatch(new GetTechnologiesSuccess(technologiesList));
    });
    this.socket.on('metric', (metric: any) => {
        this.store.dispatch(new GetMetric(metric));
    });

    this.socket.on('err', (err) => {
        this.popUp.err(err);
    });

    this.subject.pipe(concatMap(value => of(value).pipe(delay(500)))).subscribe((unit: IUnit) => {
        if (unit.userId !== sessionStorage.getItem('id')) {
            this.store.dispatch(new GetEnemyMove(unit));
        } else {
            this.store.dispatch(new GetUnitMove(unit));
        }
    });
  }

  public start() {
      this.socket.emit('user', {user: sessionStorage.getItem('id')});
  }

  exc(x: number, y: number, temp: Array<any>) {
      if ((x >= 0 && y >= 0) && (x <= 9 && y <= 9) ) {
          temp.push({x, y});
      }
  }

  getCloses(x: number, y: number) {
      const temp = [];
      this.exc(x - 1, y - 1, temp);
      this.exc(x, y - 1, temp);
      this.exc(x + 1, y - 1, temp);
      this.exc(x - 1, y, temp);
      this.exc(x + 1, y, temp);
      this.exc(x - 1, y + 1, temp);
      this.exc(x, y + 1, temp);
      this.exc(x + 1, y + 1, temp);

      return temp;
  }
}
