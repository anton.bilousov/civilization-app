import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IField, IPosition} from '../game.interface';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FieldComponent implements OnInit {
  @Input() field: IField;
  @Input() position: IPosition;
  @Output() fieldOnClick = new EventEmitter<IPosition>();
  constructor() { }

  ngOnInit() {
  }
  public takeField() {
    this.fieldOnClick.emit(this.position);
  }
}
