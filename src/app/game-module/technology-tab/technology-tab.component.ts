import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {IAppState} from '../store/state/app.state';
import {selectTechnology} from '../store/selectors/technologies.selector';
import {ITechnologies} from '../game.interface';
import {SocketService} from '../services/socket.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-history-tab',
  templateUrl: './technology-tab.component.html',
  styleUrls: ['./technology-tab.component.scss']
})
export class TechnologyTabComponent implements OnInit {
  public techList: Array<Array<ITechnologies>> = [[]];

  constructor(private store: Store<IAppState>, private service: SocketService, private location: Location) {
    this.store.select(selectTechnology).subscribe((techList: Array<ITechnologies>) => {
      // this.techList = techList.sort((a, b) => a.parent - b.children);
      let i = 0;
      let flag = true;
      const sortTechList = [];
      while (flag) {
        const list = techList.filter((val) => val.parent === i);

        if (list.length) {
          sortTechList.push(list);
          i++;
        } else {
          flag = false;
        }
      }

      this.techList = sortTechList;
    });
  }

  ngOnInit() {
  }

  public back() {
    this.location.back();
  }

  public addTech(tech: ITechnologies) {
    if (!tech.active && !tech.inUse) {
      this.service.socket.emit('technology', tech);
    }
  }

}
