import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnologyTabComponent } from './technology-tab.component';

describe('HistoryTabComponent', () => {
  let component: TechnologyTabComponent;
  let fixture: ComponentFixture<TechnologyTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnologyTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnologyTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
